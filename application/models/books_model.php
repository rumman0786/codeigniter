<?php
class Books_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}
	function general(){
		$this->load->library('MyMenu');
		$menu = new MyMenu;
		$data['base']       = $this->config->item('base_url');
		// $data['css']        = $this->config->item('css');     
		$data['bootstrap']  = $this->config->item('bootstrap');     
		$data['menu'] 		= $menu->show_menu();
		$data['webtitle']	= 'Book Collection';
		$data['websubtitle']= 'We collect all title of books on the world';
		$data['webfooter']	= '© copyright by step by step php tutorial';				
		
		$data['forminput']  = 'Form Input';
		$data['title']	 	= 'Title';
		$data['author']	 	= 'Author';
		$data['publisher']	= 'Publisher';				
		$data['year']	 	= 'Year';
		$data['years']	 	= array('2007'=>'2007',
			'2008'=>'2008',
			'2009'=>'2009',
			'2010'=>'2010',
			'2011'=>'2011',
			'2012'=>'2012',
			'2013'=>'2013',
			'2014'=>'2014'
			);
		$data['available']	= 'Available';	
		$data['summary']	= 'Summary';

		$data['ftitle'] 	= array('name' => 'title',
			'size' => 30 );
		$data['fauthor'] 	= array('name' => 'author',
			'size' => 30 );
		$data['fpublisher'] 	= array('name' => 'publisher',
			'size' => 30 );
		$data['favailable'] 	= array('name' => 'available',
			'value' => TRUE,
			'size' => 30 );
		$data['fsummary'] 	= array('name' => 'summary',
			'rows' => 5,
			'col' => 30 );
		return $data ;
	}
	function entry_insert(){
		// $this->load->database();
		$data = array('title' => $this->input->post('ftitle'),
			'author' => $this->input->post('fauthor'),
			'publisher' => $this->input->post('fpublisher'),
			'year' => $this->input->post('year'),
			'available' => $this->input->post('favailable'),
			'summary' => $this->input->post('fsummary'),
			);
		$this->db->insert('books',$data);
	}

	function entry_edit($id){
		// $this->load->database();
		$editId = $this->input->post('id') ;
		$data = array(
			
			'title' => $this->input->post('ftitle'),
			'author' => $this->input->post('fauthor'),
			'publisher' => $this->input->post('fpublisher'),
			'year' => $this->input->post('year'),
			'available' => $this->input->post('favailable'),
			'summary' => $this->input->post('fsummary'),
			);
		$this->db->update('books',$data,array('id'=>$editId));
		redirect('/books/main/', 'refresh');
	}

	function getall(){
		// $this->load->database();
		$this->load->library('table');
		$query = $this->db->query('SELECT * FROM books');
		// $table = $this->table->generate($query);
		// return $table;
		return $query->result();
	}
	//
	function get($id){
		// $this->load->database();
		$query = $this->db->get_where('books',array('id'=>$id));
		return $query->row_array();		  
	}
	function deleteEntry($id){
		var_dump($id);
		echo "DATA:: ".$id;
		$query = $this->db->delete('books', array('id' => $id)); 
		redirect('/books/main/', 'refresh');
	}
}
?>