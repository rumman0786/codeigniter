<?php
class Student_model extends CI_Model{
	function __construct()
	{
	parent::__construct();
	}

	function getStudentDetails()
	{
	$this->load->database();
	$query = $this->db->get('ci_table');
	return $query->result();
	}

	function getSingleStudentDetail()
	{
		$this->load->database();
		$query = $this->db->get_where('ci_table',array('id'=>1));
		return $query->row_array();
	} 
	function getSpecificStudentDetail($id)
	{
		$this->load->database();
		$query = $this->db->get_where('ci_table',array('id'=>$id));
		return $query->row_array();	
	}
}
?>