<?php
class student extends CI_Controller{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->load->model('Student_model');
		$data['query'] = $this->Student_model->getStudentDetails();
		$this->load->view('student_view_all',$data);
	}

	function getTheOne(){
		$this->load->model('Student_model');
		$data['query'] = $this->Student_model->getSingleStudentDetail();
		$this->load->view('student_view_one',$data);
	}
	function getAnyOne($id){
		$this->load->model('Student_model');
		$data['query'] = $this->Student_model->getSpecificStudentDetail($id);
		$this->load->view('student_view_one',$data);
	}
}
?>