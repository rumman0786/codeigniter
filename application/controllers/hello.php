<?php 
class Hello extends CI_Controller{
	var $name;
	var $color;
	function __construct()
    {
        parent::__construct();
        $this->name = "BOB";
        $this->color = "RED";
    }
	function you($name='',$color=''){
		$data['name'] = ($name) ? $name : $this->name; 
		$data['color'] = ($color) ? $color : $this->color; 
		$this->load->view('you_view',$data);

	}
}
?>