<?php
class Books extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('MyMenu');
		$this->load->model('Books_model');
	}
	
 	// function main(){
  // 		$this->load->view('books_main');
 	// }
	
 	// function input(){
  // 		$this->load->view('books_input');	
 	// }

	function main(){
		$this->load->model('Books_model');
		$data = $this->Books_model->general();
  		// $data['table'] = $this->Books_model->getall();
		$data['query'] = $this->Books_model->getall();
		$this->load->view('books_main',$data);

	}
	
	function input($id = 0){	
		// var_dump($id);
		$this->load->helper('form');
		$this->load->helper('html');
		// $this->load->model('Books_model');
		if($this->input->post('submit')){
			// echo 'going to submit';
			$this->Books_model->entry_insert();
		}else if($this->input->post('edit')){
			// echo 'going to edit '.$id;
			$this->Books_model->entry_edit('id');
		}
		$data = $this->Books_model->general();
		$data['exists'] = FALSE ;
		if((int)$id > 0){
			$query = $this->Books_model->get($id);
			$data['fid']['value'] = $query['id'];
			$data['ftitle']['value'] = $query['title'];
			$data['fauthor']['value'] = $query['author'];
			$data['fpublisher']['value'] = $query['publisher'];
			$data['fyear']['value'] = $query['year'];
			$data['exists'] = TRUE ;
			if($query['available']=='yes'){
				$data['favailable']['checked'] = TRUE;
			}else{
				$data['favailable']['checked'] = FALSE;	  
			}
			$data['fsummary']['value'] = $query['summary'];
		}
		$this->load->view('books_input',$data);	
	}

	function delete($deleteId){
		$this->Books_model->deleteEntry($deleteId);
	}
}
?>