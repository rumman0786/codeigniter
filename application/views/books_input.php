<html>
<head>
	<link rel="stylesheet" type="text/css" 
	href="<?php echo "$base/$bootstrap"?>">
</head>
<div class="container">
<body>
	<div id="header">
		<?php $this->load->view('books_header'); ?>
	</div>
	<div id="menu">
		<?php $this->load->view('books_menu'); ?>
	</div>
	<?php echo heading($forminput,3)?>

	<?php 
	if($exists == TRUE){
	$attributes = array('role' => 'form');
	// $element_attributes = array('class' => 'btn btn-default');
	 ?>
	
	<?php echo form_open('books/input/'.$fid['value'],'attributes'); ?>
	<?php echo form_hidden('id',$fid['value']); ?>
	<div class="form-group">
	<?php echo $title." : ".form_input('ftitle',$ftitle['value']).br(); ?>
	</div><div class="form-group">
	<?php echo $author." : ".form_input('fauthor',$fauthor['value']).br(); ?>
	</div><div class="form-group">
	<?php echo $publisher." : ".form_input('fpublisher',$fpublisher['value']).br(); ?>
	</div><div class="form-group">
	<?php echo $year." : ".form_dropdown('year',$years,$fyear['value']).br(); ?>
	</div><div class="checkbox">
	<?php echo $available."  : ".form_checkbox('favailable','yes',TRUE).br(); ?>
	</div><div class="form-group">
	<?php echo $summary." : ".form_textarea('fsummary',$fsummary['value']).br(); ?>
	</div><div class="form-group">
	<?php echo form_submit('edit','Edit!'); ?>
	</div>
	<?php
}else{
	?>
	<?php echo form_open('books/input'); ?>
	<?php echo $title." : ".form_input('ftitle').br(); ?>
	<?php echo $author." : ".form_input('fauthor').br(); ?>
	<?php echo $publisher." : ".form_input('fpublisher').br(); ?>
	<?php echo $year." : ".form_dropdown('year',$years).br(); ?>
	<?php echo $available." : ".form_checkbox('favailable','yes',TRUE).br(); ?>
	<?php echo $summary." : ".form_textarea('fsummary').br(); ?>
	<?php echo form_submit('submit','Submit!'); ?>
	<?php
}

echo form_close();
?>

<div id="footer">
	<?php $this->load->view('books_footer'); ?>

</div>

</body>
</div>
</html>