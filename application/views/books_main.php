<html>
<head>
	<link rel="stylesheet" type="text/css" 
	href="<?php echo "$base/$bootstrap"?>">
</head>
<div class="container">
<body>
	<div id="header">
		<?php $this->load->view('books_header'); ?>
	</div>
	<div id="menu">
		<?php echo $this->load->view('books_menu'); ?>
	</div>
	<div > 
	<table class="table table-responsive table-striped table-bordered table-hover" border="2">
		<tr>
			<th>Title</th>
			<th>Author</th>
			<th>Publisher</th>
			<th>Year</th>
			<th>Availibity</th>
			<th>Summary</th>
			<th colspan="2">Action</th>
		</tr>
		<?php 
		foreach ($query as $row) {
			?>
			<tr>
				<td><?php echo $row->title; ?> </td>
				<td><?php echo $row->author; ?> </td>
				<td><?php echo $row->publisher; ?> </td>
				<td><?php echo $row->year; ?> </td>
				<td><?php echo $row->available; ?> </td>
				<td><?php echo $row->summary; ?> </td>
				<td><?php echo anchor('books/input/'.$row->id,'Edit') ?> </td>
				<td><?php echo anchor('books/delete/'.$row->id,'Delete') ?></td>
			</tr>
			<?php
		}
		?>
	</table>
	</div>
</br>
<!-- <div class="footer"> -->
<footer><?php echo  $this->load->view('books_footer'); ?></footer>
<!-- </div> -->

</body>
</div>
</html>


